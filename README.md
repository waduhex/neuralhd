Source code for Edge-based Hyperdimensional Learning System with Brain-Like Neural Adaptation

### Intro ####

NeuralHD is a new Hyperdimensional Computing Approach with a dynamic encoder for adaptive learning. Inspired by human neuronal regeneration study in neuroscience, NeuralHD identifies insignificant dimensions and regenerates those dimensions to enhance the learning capability and  robustness. 

### Author ####

Zhuowen Zou, Mohsen Imani

### Usage ####

1. Check in Config.py that the data_location is pointing to the directory of the datasets
2. run jupyter notebook "Paper_Ready.ipynb"

### Citation Request ####

If you use NeuralHD code, please cite the following paper:

Z. Zou, Y. Kim, F. Imani, H. Alimohamadi, R. Rosario, M. Imani, “Edge-based Hyperdimensional Learning System with Brain-Like Neural Adaptation”, The International Conference for High Performance Computing, Networking, Storage, and Analysis (SC), 2021
